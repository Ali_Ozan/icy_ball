using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;

public enum CanvasType 
{   
    MainMenu,
    ModeSelection,
    Play,
    Win,
    Lose,
    Credits
}

public class CanvasManager : MonoBehaviour
{
    List<CanvasController> canvasControllerList;
    public static CanvasManager Instance;
    public GameObject grayPanel;

    public TextMeshProUGUI winTime;


    private void Awake()
    {
        canvasControllerList = GetComponentsInChildren<CanvasController>().ToList(); //GetComponentsInChildren returns an array, system.linq allows .ToList()

        canvasControllerList.ForEach(x => x.gameObject.SetActive(false));

        SwitchCanvas(CanvasType.MainMenu);
        Instance = this;
    }

    public void SwitchCanvas(CanvasType _type)
    {
        foreach (CanvasController CC in canvasControllerList)
        {
            if (CC.panelsCanvas == _type)
                CC.gameObject.SetActive(true);
            else
                CC.gameObject.SetActive(false);
        }

        if (_type == CanvasType.Play)
        {
            GameManager.Instance.InitializeGame();
            grayPanel.SetActive(false);
        }
        else if(_type == CanvasType.Win)
        {
            winTime.text = GameManager.Instance.GetTimeTxt().text;
        }
        else
            grayPanel.SetActive(true);
    }
}
