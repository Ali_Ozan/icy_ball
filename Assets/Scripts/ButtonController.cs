using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonController : MonoBehaviour
{
    internal enum buttonType {modeSelection, other};
    [SerializeField] internal buttonType btnType;

    [SerializeField] GameMode modeToSelect;
    public CanvasType desiredCanvasType;
    CanvasManager canvasManager;
    Button button;

    private void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(OnButtonClicked);
        canvasManager = CanvasManager.Instance;
    }

    public void OnButtonClicked()
    {
        switch (btnType)
        {
            case buttonType.modeSelection:
                GameManager.Instance.UpdateGameMode(modeToSelect);
                canvasManager.SwitchCanvas(desiredCanvasType);
                break;
            case buttonType.other:
                canvasManager.SwitchCanvas(desiredCanvasType);
                break;
        }
    }
}
