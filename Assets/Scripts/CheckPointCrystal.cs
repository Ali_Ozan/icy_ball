using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointCrystal : MonoBehaviour
{
    public float rotationDegreesPerSecond = 20f;
    public float hoveringAmplitude = 1f;
    public float hoveringFrequency = 1f;

    private Vector3 startPosition;

    private void Start()
    {
        startPosition = transform.position;
    }

    void FixedUpdate()
    {
        transform.Rotate(new Vector3(0f, 0f, rotationDegreesPerSecond * Time.deltaTime));
        transform.position = startPosition + new Vector3(0f, Mathf.Sin(Time.time * hoveringFrequency) * Time.deltaTime * hoveringAmplitude, 0f);
    }
}
