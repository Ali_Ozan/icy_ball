using UnityEngine.InputSystem;
using UnityEditor;
using UnityEngine.InputSystem.Controls;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.InputSystem.Utilities;

public struct PlatformState:IInputStateTypeInfo
{
    public FourCC format => new FourCC(a: 'M', b: 'Y', c: 'D', d: 'V');


    //[InputControl](NameAndParameters = "platformControl")

    [InputControl(name = "position/x", format = "SHRT")]
    public short x;
    [InputControl(name = "position/y", format = "SHRT", offset = 2)]
    public short y;
}


[InputControlLayout(stateType = typeof(PlatformState))]
public class Platform : InputDevice
{
    /* private void OnEnable()
     {
         var joystick = InputSystem.AddDevice<Gamepad>();
         InputSystem.QueueStateEvent(joystick, new GamepadState() { leftStick = new Vector2(x: 0.5f, y: 0.5f) });
     }*/

    [InputControl]
    public Vector2Control platformControl { get; private set; }


    protected override void FinishSetup()
    {
        base.FinishSetup();

        platformControl = GetChildControl<Vector2Control>(path: "platformControl");
    }

    static Platform()
    {
        InputSystem.RegisterLayout<Platform>(matches: new InputDeviceMatcher().WithInterface(pattern: "Platform"));

        //if (!InputSystem.devices.Any(x:InputDevice => x is Platform))
        {
            InputSystem.AddDevice<Platform>();
            //InputSystem.AddDevice(new InputDeviceDescription { interfaceName = "Platform", product = "Bilintek" });
        }


    }

    [MenuItem("Tools/Add Platform")]
    public static void Initialize()
    {
        InputSystem.AddDevice<Platform>();
    }
}
