using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using System;

public class PlayerController : MonoBehaviour
{
    private Rigidbody rb;
    Vector2 inputVector; // x = sideways movement, y = front and back movement
    public float speedMultiplier = 1f;

    public static PlayerController Instance;
    private Vector3 respawnPosition;
    private Vector3 startPosition;

    public GameObject collectibleAnimation;
    public GameObject checkpointAnimation;

    void Start()
    {
        Instance = this;
        rb = GetComponent<Rigidbody>();
        respawnPosition = transform.position;
        startPosition = transform.position;
    }

    private void OnMove(InputValue inputValue) //when any kind of controller is used this function will be called (OnMove = any kind of change in the movement inputs)
    {
        if (GameManager.Instance.gameIsON == true)
        {
            GameManager.Instance.counterIsOn = true;
            inputVector = inputValue.Get<Vector2>();
        }
    }

    void Update()
    {
        
    }

    void FixedUpdate()
    {
        if (GameManager.Instance.gameIsON)
        {
            rb.AddForce((new Vector3(inputVector.x, 0.0f, inputVector.y)) * speedMultiplier);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Collectible"))
        {
            Instantiate(collectibleAnimation, other.transform.position, other.transform.rotation);
            other.gameObject.SetActive(false);
            GameManager.Instance.UpdateCollectibles();
        }
        if (other.gameObject.CompareTag("Checkpoint"))
        {
            Instantiate(checkpointAnimation, other.transform.position, other.transform.rotation);
            other.gameObject.SetActive(false);
            GameManager.Instance.UpdateCheckpoints();
            respawnPosition = other.transform.position;
        }
        if (other.gameObject.CompareTag("DyingTrigger"))
        {
            transform.position = respawnPosition;
            rb.velocity = new Vector3(0f, 0f, 0f);
            GameManager.Instance.LoseLife();
        }
        if (other.gameObject.CompareTag("Finish"))
        {
            GameManager.Instance.gameIsON = false;
            other.gameObject.GetComponent<AudioSource>().Play();
            Color emissiveColor = other.gameObject.GetComponent<MeshRenderer>().material.GetColor("_EmissionColor");
            other.gameObject.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", emissiveColor * 3f);
            GameManager.Instance.Win();
        }
    }

    public void SetToStartPosition()
    {
        transform.position = startPosition;
        rb.velocity = new Vector3(0f, 0f, 0f);
    }
}
