using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationObjectKiller : MonoBehaviour
{

    private ParticleSystem ps;

    void Start()
    {
        ps = transform.GetComponent<ParticleSystem>();
        Invoke("Die", ps.startLifetime);
    }


    void Die()
    {
        Destroy(gameObject);
    }
}
