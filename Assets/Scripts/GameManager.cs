﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using System;

public enum GameMode
{
    PracticeMode,
    Classic,
    TimeChallenge,
    OneShot
}

public class GameManager : MonoBehaviour
{
    public static GameManager Instance; //static instance will let me grab it from anywhere

    public GameMode mode;

    public GameObject LvlPrefab;
    private GameObject currentLVL;
    private bool firstStart = true;

    private int collectibleCount = 0;
    private int totalCollectibleCount;
    
    private int collectedCheckpoints = 0;
    private int totalCheckpointCount;
    
    private int livesLeft;
    public int totalLives = 3;
    
    private float time = 0f;
    public float challengeTime = 180f;

    public TextMeshProUGUI currentModeTxt;
    public TextMeshProUGUI collectiblesText;
    public TextMeshProUGUI checkpointsText;
    public TextMeshProUGUI livesText;

    private GameObject collectiblesParent;
    private GameObject checkpointsParent;

    public GameObject collectibleAnimation;
    public GameObject checkpointAnimation;

    public TextMeshProUGUI TimeTxt;
    private TimeSpan timeSpan;

    public TextMeshProUGUI endGameCollectedTxt;
    public TextMeshProUGUI endGameLivesTxt;

    public bool gameIsON = false;
    public bool counterIsOn = false;


    void Awake()
    {
        Instance = this;
        mode = GameMode.Classic;
    }


    void Start()
    {
        //InitializeGame();
        currentModeTxt.text = "CURRENT MODE: " + "CLASSIC";
        currentLVL = GameObject.Find("LEVEL");
    } 

    public void InitializeGame()
    {
        ResetLevel();
        gameIsON = true;

        switch (mode)
        {
            case GameMode.PracticeMode:
                livesLeft = 1;
                totalLives = 1;
                time = 0f;
                break;
            case GameMode.Classic:
                livesLeft = 3;
                totalLives = 3;
                time = 0f;
                break;
            case GameMode.TimeChallenge:
                livesLeft = 3;
                totalLives = 3;
                time = challengeTime;
                break;
            case GameMode.OneShot:
                livesLeft = 1;
                totalLives = 1;
                time = 0f;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(mode), mode, null);
        }


        collectibleCount = 0;
        totalCollectibleCount = collectiblesParent.transform.childCount;
        collectiblesText.text = "COLLECTIBLES: " + collectibleCount.ToString() + "/" + totalCollectibleCount.ToString();

        totalCheckpointCount = checkpointsParent.transform.childCount;
        checkpointsText.text = "CHECKPOINTS: " + collectedCheckpoints.ToString() + "/" + totalCheckpointCount.ToString();

        if (mode == GameMode.PracticeMode)
            livesText.text = "LIVES: " + "∞";
        else
            livesText.text = "LIVES: " + livesLeft.ToString() + "/" + totalLives.ToString();

        timeSpan = TimeSpan.FromSeconds(time);
        TimeTxt.text = timeSpan.Minutes.ToString() + ":" + timeSpan.Seconds.ToString() + ":" + timeSpan.Milliseconds.ToString();
    }

    void Update()
    {
        if (counterIsOn)
        {
            if (mode == GameMode.PracticeMode ||
                mode == GameMode.Classic ||
                mode == GameMode.OneShot)
            {
                CountTime();
            }
            else if (mode == GameMode.TimeChallenge)
                CountdownTime();
        }
    }

    private void CountdownTime()
    {
        time = time - Time.deltaTime; //time = time left in this countdown case
        timeSpan = TimeSpan.FromSeconds(time);
        TimeTxt.text = timeSpan.Minutes.ToString() + ":" + timeSpan.Seconds.ToString() + ":" + timeSpan.Milliseconds.ToString();
    }

    private void CountTime()
    {
        time = time + Time.deltaTime; //time = passed time in this case
        timeSpan = TimeSpan.FromSeconds(time);
        TimeTxt.text = timeSpan.Minutes.ToString() + ":" + timeSpan.Seconds.ToString() + ":" + timeSpan.Milliseconds.ToString();
    }

    public void UpdateGameMode(GameMode newMode)
    {
        mode = newMode;
        
        switch (newMode)  {
            case GameMode.PracticeMode:
                currentModeTxt.text = "CURRENT MODE: " + "PRACTICE";
                break;
            case GameMode.Classic:
                currentModeTxt.text = "CURRENT MODE: " + "CLASSIC";
                break;
            case GameMode.TimeChallenge:
                currentModeTxt.text = "CURRENT MODE: " + "TIME CHALLENGE";
                break;
            case GameMode.OneShot:
                currentModeTxt.text = "CURRENT MODE: " + "ONE SHOT";
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(newMode), newMode, null);
        }
    }
    

    public void UpdateCollectibles()
    {
        collectibleCount++;
        collectiblesText.text = "COLLECTIBLES: " + collectibleCount.ToString() + "/" + totalCollectibleCount.ToString();
    }

    public void UpdateCheckpoints()
    {
        collectedCheckpoints++;
        checkpointsText.text = "CHECKPOINTS: " + collectedCheckpoints.ToString() + "/" + totalCheckpointCount.ToString();
    }

    public void LoseLife()
    {
        if (mode != GameMode.PracticeMode)
            livesLeft--;

        livesText.text = "LIVES: " + livesLeft.ToString() + "/" + totalLives.ToString();
        if (livesLeft == 0)
        {
            gameIsON = false;
            counterIsOn = false;
            time = 0f;
            CanvasManager.Instance.SwitchCanvas(CanvasType.Lose);
        }
    }

    public void Win()
    {
        Invoke("ShowWinPanel", 0.5f);
    }

    private void ShowWinPanel()
    {
        counterIsOn = false;
        gameIsON = false;
        time = 0f;
        CanvasManager.Instance.SwitchCanvas(CanvasType.Win);
        endGameCollectedTxt.text = collectibleCount.ToString() + "/" + totalCollectibleCount.ToString();
        endGameLivesTxt.text = livesLeft.ToString() + "/" + totalLives.ToString();
    }

    private void ResetLevel() //Tried to reset the scene without reloading, couldn't manage, might try some other way later.
    {
        if (firstStart == true)
            firstStart = false;
        else
            currentLVL = GameObject.Find("LEVEL(Clone)");

        Destroy(currentLVL);
        Instantiate(LvlPrefab);
        collectiblesParent = GameObject.Find("COLLECTIBLES");
        checkpointsParent = GameObject.Find("CHECKPOINTS");
        PlayerController.Instance.SetToStartPosition();
    }

    public TextMeshProUGUI GetTimeTxt()
    {
        return TimeTxt;
    }
}


