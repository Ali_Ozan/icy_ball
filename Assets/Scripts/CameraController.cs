using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject player;

    Vector3 offset;

    void Start()
    {
        //offset = transform.position - player.transform.position;
        offset = new Vector3(0f, 11f, -7f);
    }

    void LateUpdate() //for follow cameras and tasks like gathering last known states, LateUpdate is more suitable. Because other update functions(or physics system) might actually change the parameter that this "dependent" tasks rely on.
    {
        Vector3 targetPosition = player.transform.position + offset;
        //transform.position = targetPosition;
        transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * 3f);
    }
}
